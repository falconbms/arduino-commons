# README #

# STEPPER Serial command example #
{"setup_stepper":{"rpm":{"steps":600,"pins":[14,15,16,17],"maxSpeed":500,"acceleration":500}}}
{"setstep":{"rpm":210}}


# DISPLAY Serial command example #
{"setup_display":{"DED":{"pins":[52,51,10,9,8],"model":"SSD1322"}}}
{"setup_display":{"DED":{"pins":[52,51,10,9,8],"model":"SSD1322"},"FFI":{"pins":[52,51,5,4,3],"model":"SH1106"},"PFL":{"pins":[52,51,6,7,8],"model":"SSD1322"}}}
{"set_display":{"DED":["229"]}}
{"set_display":{"DED":[" UHF  292.30   STPT $ 8",""," VHF   1        10:56:20",""," M1 3 C  6400  MAN T 75X"]}}
{"set_display":{"DED":["       CRUS   %TOS%  8 $","       SYSTEM  10:56:23","    DES TOS    10:57:51","        ETA    10:58:42","    RQD G/S    516KTS"]}}
{"set_display":{"DED":["        STPT %!^%$  MAN","     LAT N  35° 58.060'","     LNG E 127° 25.312'","    ELEV     -0FT","     TOS  10:57:51"]}}


# LIGHTBIT Serial command example #
{"setup_LightBit":{"pins":[4,5,6,7,8,9,10,11,12,13,22,23,24,25]}}
{"set_LightBit":{"mode":[0,0,0,0,0,0,0,0,0,0,0,0,0,0]}}
{"set_LightBit":{"mode":[1,1,1,1,1,1,1,1,1,1,1,1,1,1]}}
{"set_LightBit":{"mode":[0,0,0,0,0,0,0,0,0,0,0,0,0,0]}}


# MATRIXLIGHTBIT Serial command example #
{"setup_matrix":{"A":{"rows":8,"cols":4,"pinStart":2}}}
{"update_matrix":{"A":{"matrice":[[1,1,0,0],[1,0,0,1],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]}}}
{"destroy_matrix":{}}}



# BCD DECODER Serial command example #

{"setup_bcd":{"7_uhf_chan":{"bcd_pins":[2,3,4,5],"seg_pins":[12,13],"name":"7_uhf_chan"},"7_uhf_freq":{"bcd_pins":[2,3,4,5],"seg_pins":[6,7,8,9,10,11],"name":"7_uhf_freq","dot":30}},"digitOff":100,"digitOn":2000}
{"set_seg":{"value":[3,4,0,1,5,9],"name":"7_uhf_freq"}}}
{"set_seg":{"value":[3,8],"name":"7_uhf_chan"}}}
{"set_seg":{"value":[1,1],"name":"7_uhf_chan"}}}


{"setup_bcd":{"7_uhf_chan":{"bcd_pins":[2,3,4,5],"seg_pins":[6,7],"name":"7_uhf_chan"}},"digitOff":200,"digitOn":3000}
{"set_seg":{"value":[2,5],"name":"7_uhf_chan"}}}


{"setup_bcd":{"7_uhf_chan":{"bcd_pins":[2,3,4,5],"seg_pins":[6,7,8,9,10,11],"name":"7_uhf_chan"}},"digitOff":200,"digitOn":3000}
{"set_seg":{"value":[2,3,4,5],"name":"7_uhf_chan"}}}

{"destroy_all_bcd":"true"}




{"setup_bcd":{"7_uhf_chan":{"bcd_pins":[2,3,4,5],"seg_pins":[12,13],"name":"7_uhf_chan","dot":0},"7_uhf_freq":{"bcd_pins":[2,3,4,5],"seg_pins":[6,7,8,9,10,11],"name":"7_uhf_freq","dot":14}},"digitOff":200,"digitOn":3000}


{"setup_bcd":{"7_uhf_chan":{"bcd_pins":[2,3,4,5],"seg_pins":[12,13],"name":"7_uhf_chan","dot":0},"7_uhf_freq":{"bcd_pins":[2,3,4,5],"seg_pins":[6,7,8,9,10,11],"name":"7_uhf_freq","dot":14}},"digitOff":10000,"digitOn":10000}



{"set_seg":{"name":"7_uhf_freq","value":[]}}
{"set_seg":{"name":"7_uhf_chan","value":[]}}

{"set_seg":{"name":"7_uhf_freq","value":[3,2,5,0,0,0]}}
{"set_seg":{"name":"7_uhf_chan","value":[44]}}
